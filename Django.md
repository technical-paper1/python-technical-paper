# Django 

- Django is a high-level, open-source web framework written in Python.
- Django is designed to be easy to use and is a good choice for building web applications.
- Django is a popular choice for web development because it is easy to learn and has a large.
- It follows the `Model-View-Controller` (MVC).
- In the Django world, it's often referred to as the `Model-View-Template` (MVT) pattern.

## Settings
- Django settings are a collection of variables that define the behavior of a Django project.
- The `settings` module contains all the configuration information for your Django application.
- Django settings are divided into two types: global settings and local settings.
- The `settings.py` file is the main settings file for your Django project.

#### Database Configuration:

- The DATABASES setting defines the configuration for your database connection. 
- You specify the database engine, name, user, password, and other relevant details.

#### Debug Mode:
- The DEBUG setting controls whether your application is in debug mode or not. 
- In debug mode, you get more detailed error pages and additional debugging information.

#### Secret Key:
- The SECRET_KEY setting is a crucial security measure. It's used for cryptographic signing and should be kept secret.
- It's used for various security-related purposes, such as creating session keys and CSRF tokens.

#### Allowed Hosts:
- The ALLOWED_HOSTS setting specifies a list of valid host/domain names that this Django site can serve.
- This is a security measure to prevent HTTP Host header attacks.

#### Static and Media Files:
- Settings like STATIC_URL, STATICFILES_DIRS, MEDIA_URL, and MEDIA_ROOT are used to configure the serving of static and media files. 
- Static files include CSS, JavaScript, and image files.

## URLS

- urls.py is the file where you define all the url patterns for your app.
- This maps urls with views where all the business logic is written.

#### Include Function:
- The include function is used to include other URL pattern sets from different modules. 
- This is useful for organizing and modularizing your URL patterns.

    ```python
    from django.urls import path, include
    from . import views

    urlpatterns = [
        path('app/', include('myapp.urls')),
    ]
    ```

#### Dynamic URL Parameters:
- Dynamic URL parameters can be specified using angle brackets < >. 
- These parameters are captured and passed to the associated view function.

    ```python
    from django.urls import path
    from . import views

    urlpatterns = [
        path('articles/<int:article_id>/', views.article_detail),
    ]
    ```

## Views

- This is where all the business logic is written which handle all the web requests.
- Django supports both function-based views and class-based views. Function-based views are simple functions that take a web request and return a web response.

### Class Based

- Defined as Python classes.
- Different HTTP methods (e.g., get, post) are handled by separate methods within the class.
- Encourages code organization and reusability.
- Supports inheritance, making it easy to extend and customize views.
  
    ```python
    from django.views import View
    from django.http import HttpResponse

    class MyView(View):
        def get(self, request):
            return HttpResponse("Hello, Django!")
    ```

### Function Based

- Defined as simple Python functions.
- Accepts a request object as an argument and returns an HTTP response.
- Suitable for small and concise projects.

    ```python
    from django.shortcuts import render
    from django.http import HttpResponse

    def my_view(request):
        return HttpResponse("Hello, Django!")
    ```


## Templates

- Templates are used to generate dynamic HTML content by combining HTML code with Python-like expressions and control structures. 
- Templates allow you to separate the presentation logic from the business logic in your web application.

#### Variable Rendering:
- Variables are enclosed in double curly braces {{ variable }}.
- Example: `{{ user.username }}`.

#### Tags:
- Tags provide control flow and logic in templates.
- Some tags are
  - extends
  - include
  - for
  - if
  - comment
  - block
- Example: `{% if user.is_authenticated %} ... {% endif %}`.

#### Filters:
- Filters modify the output of variables or tags.
- Example: `{{ value|filter_name }}`.

## Static files

- In Django, static files are used to hold files like CSS, JavaScript and other files which do not change during the execution of app.
- The `STATICFILES_DIRS` setting specifies additional directories to look for static files.
- The `STATIC_URL` setting defines the base URL for serving static files.
- To load a static file `{% load static %}` keyword is used.
- For example,
    ```html
    {% load static %}

    <link rel="stylesheet" type="text/css" href="{% static 'myapp/css/styles.css' %}">
    ```

## Models
- Models are Django's way of storing data as a SQL database.
- Django models simplify the tasks and organize tables into models.
- Each attribute of the model represents a database field. 
    ```python
    from django.db import models

    class Book(models.Model):
        title = models.CharField(max_length = 200)
        author=models.CharField(max_length=100)
        description = models.TextField()
    ```


## Migration

- After you have created a model in Django, you actually need to use two commands.
- These commands basically convert the Django model type to SQL queries which thereafter converts it as a database.
    ```python
    python3 manage.py makemigrations
    ```
    - This will stage all the changes in the models.py
    ```python
    python3 manage.py migrate
    ```
    - This is similar to the `commit` command we use to commit all the changes in Git and the earlier command is similar to `add` in Git.
- To make all the models visible on the admin page we need to register the model in the admin.py file first.
    ```python
    from django.contrib import admin 
    from .models import Book 

    # Register your models here. 
    admin.site.register(Book)
    ```

## ORM
- Django's Object-Relational Mapping (ORM) is a way to interact with database models.
- We can access ORM through python shell, run the following command to open shell.
    ```python
    python3 manage.py shell
    ```
- You will see a python interactive console, where you can interact with the models.
    ```python
    >>> from myapp.models import Book

    >>> a=Book.objects.all()
    #this will get you all the objects inside the model Book.

    >>> b=Book.objects.create(author='A.P.J',title='The Wings of fire',description='...')
    >>> b.save() #this will create a new object of model Book.
    
    >>>c=Book.objects.filter(pk='A.P.J')##pk is the primary key here

    >>>c.delete() # this will delete the object from the model.

    #you can update the object by storing it in a variable and then updating it.
    >>>d=Book.objects.get(pk='A.P.J Abdul')
    >>>d.title='The Wings of Fire'##update title

    >>>Book.objects.all().delete()# to delete all the objects of a model
    ```

## Forms

- In Django, forms handle the user input and interact with the database.
- They provide a way to create,update and validate user input data.
- Forms in Django are created using Python classes that inherit from django.forms.Form or one of its subclasses.
- Each form class defines fields, widgets, and validation for the data it will handle. 


    ```python
    from django import forms

    class MyForm(forms.Form):
        name = forms.CharField(max_length=100)
        age = forms.IntegerField()
    ```

- You can handle form submissions as follows,
    ```python
    from django.shortcuts import render

    def my_view(request):
        if request.method == 'POST':
            form = MyForm(request.POST)
            if form.is_valid():
                # Process valid form data
            else:
                # Form is invalid; redisplay with errors
        else:
            form = MyForm()

        return render(request, 'my_template.html', {'form': form})
    ```

- You can create a form for the data models as well,
    ```python
    from django import forms
    from .models import MyModel

    class MyModelForm(forms.ModelForm):
        class Meta:
            model = MyModel
            fields = ['name', 'age']
    ```

### CSRF Token

- `Cross Site Response Forgery` (CSRF) is a type of cyber attack where the attacker pretends to be the authorized personel to access previlized information.
- Django protects against CSRF attacks by including a CSRF token in each form rendered using the `{% csrf_token %}` template tag.
- To generate a csrf token you can use the tag `{% csrf_token %}`.


    ```python
    <form method="post" action="{% url 'my_view' %}">

    {% csrf_token %}
    <!-- Other form fields go here -->

    <input type="submit" value="Submit">
    </form>
    ```


### WSGI

- WSGI, which stands for Web Server Gateway Interface, is a specification that defines how web servers communicate with web applications.


- It serves as a bridge between web servers and Python web applications or frameworks.


- WSGI provides a standard interface that promotes interoperability between web servers and Python web applications, enabling a wide range of deployment options for Python web development.



### XSS

- Cross-Site Scripting (XSS) is a type of security vulnerability that occurs when an attacker injects malicious scripts into web pages that are viewed by other users. 
- This can lead to the execution of the injected scripts in the context of the victim's browser, 
- This enables the attacker to steal sensitive information, manipulate web page content, or perform actions on behalf of the user.
- This is mostly done with user input, url parameters and through cookies too.