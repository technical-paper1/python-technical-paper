# String Methods

Python provides a wide range of built-in string methods that allow you to manipulate and work with strings.

Here are some common string methods in Python:

## upper() :

- Converts all characters in a string to uppercase.

    ```python
    text = "shaik, rakhaib!"

    upper_text = text.upper()
    ```

## lower():

* Converts all characters in a string to lowercase.

    ```python
    text = "SHAIK, RAKHAIB!"

    lower_text = text.lower()
    ```

## strip() :


* Removes leading and trailing whitespace (spaces, tabs, and newline characters) from a string.

    ```python
    text = "   this is a string with whitespace   "

    stripped_text = text.strip()
    ```

## replace():

* Replaces all occurrences of a substring old with a new substring new.
  ```python
  text = "abc, def"

  new_text = text.replace("def", "456")
  ```

## split():

* Splits a string into a list of substrings using the specified separator character (default is space).

    ```python
    text = "A,B,C"

    fruits = text.split(",")
    ```
## find():

* Finds the first occurence of an substring in the string.
* Will return -1 if not present.
  
    ```python
    name='Shaik Rakhaib'

    indexOfShaik=name.find('Shaik')
    ```
## count():

* counts the occurences of the specified value in the string.

    ```python
    text='abcabcabc'

    freOfabc=text.count('abc')
    ```

## isdigit():

* checks if the value specified is digit in string format or not.

    ```python
    num='123'

    x=num.isdigit()
    ```

## islower():

* checks if the value specified is lower or not.

    ```python
    text='abc'

    x=text.islower()
    ```
## isupper():

* checks if the value specified is upper in string format or not.

    ```python
    text='ABC'

    x=num.isupper()
    ```
