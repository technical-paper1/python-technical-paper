# Methods in Lists/Arrays



## Lists
* Lists in python are similar to arrays in C++, Java.
* Lists are used to store multiple items in a single variable.
* There are various in built methods for Lists and Arrays.
* They are represented by square brackets.

Some of them are as follows:

## append()

- You can append values to an already existing list using this method.
- The syntax is as follows,

    ```python
    lst=[10,20,30]

    lst.append(40)

    print(lst)
    ```

## clear()

* Clears the whole data inside a list.
* If you need to remove all the data from the list, you can use this method.
* Syntax for the same is as follows,

    ```python
    lst=[3,2,1]

    lst.clear()
    ```


## copy()

* Will return a copy of the list with the contents inside it.
* If you need a duplicate list with the same contents, you can use this method.
* Syntax for the same is as follows.

    ```python
    lst=[1,2,3]

    dupList=lst.copy()

    print(dupList)
    ```

## count()

* Will return the number of elements with the given specified value.
* If you need to find the frequency of elements in a list, you can use this method.
* Syntax for the same is as follows.


    ```python
    fruits=['Apple','Orange','Apple','Apple']

    countOfApple=fruits.count('Apple')
    ```
  
## extend()

* To add the elements of a list to the end of the current list.
* If you need to combine two lists or any iterable data structures, you can use this method.
* Syntax for the same is as follows,
  
    ```python
    lst=[1,2,3]

    lst1=[4,5,6]

    lst=lst.extend(lst1)

    print(lst)
    ```

## index()

* Returns the index of the first element with the specified value.
* If you need the check if an element is present in the list, you can use this method.
* Syntax for the same is as follows
    ```python
    name=["Ram","Rahim","Ramesh"]

    indexOfRahim=name.index("Rahim")
    #will give an error if not in the list.
    ```

## insert()

* Adds an element at the specifies position of the list.
* Takes two arguments, the first one being the index where you want to insert the element and second one being the element.
  
    ```python
    lst=[1,3,4]

    lst.insert(1,2)

    print(lst)
    ```
## pop()

* Pops an element at the specifies position of the list.
* Returns the popped element from the list.
  
    ```python
    lst=[1,3,4]

    lst.pop(1)
    ```

## reverse()

* Reverses the order of the list
  
  ```python
  lst=[1,2,3,4,5]

  lst.reverse()
  ```

## sort()

* Sorts the list in ascending order by default.
  ```python
  lst=[3,1,2]

  lst.sort()
  ```